# zjp

WTF

Tools for JavaScript
1. Cloc (counts)
2. Complexity report (ABC complexity)
3. JS inspect (structural similarities)
4. ESLint (static code analyzer and code formater)


Cloc (counts)

for whole howler.js
       2 text files.
       2 unique files.
       2 files ignored.
github.com/AlDanial/cloc v 1.78  T=0.04 s (48.8 files/s, 73714.9 lines/s)
-------------------------------------------------------------------------------
Language                     files          blank        comment           code
-------------------------------------------------------------------------------
JavaScript                       2            460            738           1821
-------------------------------------------------------------------------------
SUM:                             2            460            738           1821
-------------------------------------------------------------------------------

for Howl.prototype.init
       1 text file.
       1 unique file.
       0 files ignored.

github.com/AlDanial/cloc v 1.78  T=0.03 s (33.0 files/s, 923.9 lines/s)
-------------------------------------------------------------------------------
Language                     files          blank        comment           code
-------------------------------------------------------------------------------
JavaScript                       1              3              3             22
-------------------------------------------------------------------------------


Complexity report

  Physical LOC: 28
  Logical LOC: 23
  Mean parameter count: 1
  Cyclomatic complexity: 15
  Cyclomatic complexity density: 65.21739130434783%
  Maintainability index: 100.55903021038915
  Dependency count: 0

  Function: <anonymous>
    Line No.: 1
    Physical LOC: 28
    Logical LOC: 1
    Parameter count: 1
    Cyclomatic complexity: 1
    Cyclomatic complexity density: 100%
    Halstead difficulty: 1
    Halstead volume: 8
    Halstead effort: 8

  Function: <anonymous>
    Line No.: 2
    Physical LOC: 26
    Logical LOC: 20
    Parameter count: 1
    Cyclomatic complexity: 15
    Cyclomatic complexity density: 75%
    Halstead difficulty: 19
    Halstead volume: 1172.8421251514428
    Halstead effort: 22284.00037787741


JS Complexity Analysis

Summary
Maintainability 	58.7
Lines of code 		23
Difficulty 			21.7
Estimated # of Bugs 0.4

Functions
Function	SLOC	# params	Complexity	Difficulty	Est # bugs
<anonymous>	1		1			1			1			0
<anonymous>	20		1			15 			20.8		0.4


Code metrics

1 function expresion (line 2)
2 return statement (lines: 2, 26)
3 binary expressions (lines 6-8)
11 conditional expressions (lines 10-17, 21-23)

JS inspect
In whole project howler.js are 10 matches across 2 files. In function Howl.prototype.init there is no matches of structural similarities.
